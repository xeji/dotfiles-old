# list of nixpkgs user overlays - these are applied in sequence
# These are intentionally kept separate from the system-wide overlays
# in order to be independently usable on non-nixOS machines.

[

(self: super: {
  # must import without overlays to avoid infinite loop
  unstable = import <nixpkgs-unstable> { overlays = []; };
})

# locally defined packages
#(self: super: import ./pkgs { pkgs = super; })

# template for additional overrides
#(self: super: {
  # overrides: p = super.p.override {...};
  # packages from stable: p = super.stable.p;
  # inherit (super.stable) p1 p2 p3;
#})

]
