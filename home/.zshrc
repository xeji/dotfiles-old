# boring standard prompt - TODO: find a nicer one
PROMPT='%B%F{green}[%n@%m:%~]%(!.#.$)%f%b '

## aliases
alias la='ls -a'
alias sl='ls'
alias grep='grep --color=auto'
alias nixdevpath='export NIX_PATH=nixpkgs=${HOME}/nixpkgs:nixpkgs-overlays=/.emptydir'
alias ssh-gpg='source $HOME/bin/ssh-gpg.sh'
alias sp='st & disown'


# ls colors
# TODO: unify with ranger, never vivid version with more themes
if (( $+commands[vivid] )); then
  export LS_COLORS="$(vivid -m 8-bit generate ayu)"
fi


## Various plugins

# enable homeshick if installed
if [[ -f "$HOME/.homesick/repos/homeshick/homeshick.sh" ]]; then
  source "$HOME/.homesick/repos/homeshick/homeshick.sh"
fi

