"" vim-plug for imperative plugin management
if filereadable(stdpath('config') . "/plug.vim")
  source ~/.config/nvim/plug.vim
endif

"{{ basic settings
set mouse=nv
set hidden
set expandtab shiftwidth=2 softtabstop=2
set ignorecase smartcase showmatch
set termguicolors background=dark
set spelllang=de_de,en_us
set wildmode=longest:full,full  " wildmenu - does it conflict with deoplete ?
set wildignore=*.aux,*.fls,*.fdb_latexmk,*.toc,*.snm,*.nav,*.out,*.vrb
colorscheme jellybeans
"}}

"{{ custom keybindings
nnoremap <space> <nop>
let mapleader = "\<Space>"
nnoremap <c-p> :FZF<CR>
"}}

"{{ lightline
let g:lightline = {'colorscheme': 'default'}
"}}

"{{ vim-pandoc
let g:pandoc#command#templates_file = stdpath('config') . '/vim-pandoc-templates'
let g:pandoc#folding#mode = 'stacked'  " show all headers
let g:pandoc#folding#fdc = 0   " no folding column
let g:pandoc#syntax#conceal#use = 0
let g:pandoc#syntax#codeblocks#embeds#langs = ['latex=tex', 'tex', 'python', 'sh', 'bash=sh']
"}}

"{{ vimwiki
let g:vimwiki_global_ext = 0  " don't use vimwiki mode for md files outside wiki dir
let g:vimwiki_autowriteall = 0  " don't auto write, just hide buffer
let g:vimwiki_auto_header = 1
let g:vimwiki_hl_headers = 1
let g:vimwiki_hl_cb_checked = 1
let g:vimwiki_markdown_link_ext = 1
"future option: diary_frequency
let s:wiki1 = { 'path': '~/vimwiki', 'name': 'main', 'syntax': 'markdown',
  \ 'ext': '.md', 'auto_diary_index': 1, 'auto_tags':1 }
let g:vimwiki_list = [ s:wiki1 ]
"}}


"" local tweaks
if filereadable(stdpath('config') . "/local.vim")
  source ~/.config/nvim/local.vim
endif
