"" load plugins using plug.vim
call plug#begin(stdpath('data') . '/plugged')
"""""""""

"" ready
Plug 'junegunn/limelight.vim'  " dim other lines, :Limelight

"" optimize config
Plug 'lervag/vimtex'


"" add local plugins
let s:localplug = stdpath('config') . "/local-plug.vim"
if filereadable(s:localplug)
  execute 'source' s:localplug
endif

"""""""""
call plug#end()
