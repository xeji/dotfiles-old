#!/usr/bin/env bash

set -o noclobber -o noglob -o nounset -o pipefail
IFS=$'\n'

## If the option `use_preview_script` is set to `true`,
## then this script will be called and its output will be displayed in ranger.
## ANSI color codes are supported.
## STDIN is disabled, so interactive scripts won't work properly

## This script is considered a configuration file and must be updated manually.
## It will be left untouched if you upgrade ranger.

## Because of some automated testing we do on the script #'s for comments need
## to be doubled up. Code that is commented out, because it's an alternative for
## example, gets only one #.

## Meanings of exit codes:
## code | meaning    | action of ranger
## -----+------------+-------------------------------------------
## 0    | success    | Display stdout as preview
## 1    | no preview | Display no preview at all
## 2    | plain text | Display the plain content of the file
## 3    | fix width  | Don't reload when width changes
## 4    | fix height | Don't reload when height changes
## 5    | fix both   | Don't ever reload
## 6    | image      | Display the image `$IMAGE_CACHE_PATH` points to as an image preview
## 7    | image      | Display the file directly as an image

## DEPENDENCIES to preview various file types (nix packages)
# archiving : gnutar, unzip, p7zip, atool (for various archives)
# bat : syntax highlighting for text + program code
# catdoc : .doc, .xls (includes xls2csv)
# catdocx : .docx
# compression : gzip, bzip2, xz, zstd, lzip
# libcaca : img2txt text preview of images
# exiftool : show image metdata
# fontforge : fontimage for fonts
# imagemagick : convert for images
# jq : json (nicer, colored output)
# mediainfo : show media metadata
# transmission : torrents
# mupdf : mutool (pdf -> text)
# odt2txt : text preview for libreoffice docs
# pandoc : (slow) fallback for html, .odt, .docx, .epub
# w3m / lynx : html
# xlsx2csv : .xlsx

## Script arguments
FILE_PATH="${1}"         # Full path of the highlighted file
PV_WIDTH="${2}"          # Width of the preview pane (number of fitting characters)
PV_HEIGHT="${3}"         # Height of the preview pane (number of fitting characters)
IMAGE_CACHE_PATH="${4}"  # Full path that should be used to cache image preview
PV_IMAGE_ENABLED="${5}"  # 'True' if image previews are enabled, 'False' otherwise.

FILE_EXTENSION="${FILE_PATH##*.}"
FILE_BASENAME="${FILE_PATH%.*}"
if [[ ${FILE_BASENAME} =~ .tar$ ]]; then
  FILE_EXTENSION="tar.${FILE_EXTENSION}"
fi
FILE_EXTENSION_LOWER="${FILE_EXTENSION,,}"


## Settings
MAXLN=100    # Stop after $MAXLN lines

# a common post-processing function used after most commands
trim() { head -n "$MAXLN"; }

# exit with code $1 if previous command terminated successfully or with SIGPIPE
# needed for trim because we set -o pipefail
pipeexit() { [[ $? == 0 || $? == 141 ]] && exit $1 ; }


handle_extension() {
  # I prefer using mime types, so this is only used for special cases
  # like compressed archives that cannot be identified by their mime type
  case "${FILE_EXTENSION_LOWER}" in

    ## Archive files
    tar|tar.bz|tbz|tar.bz2|tbz2|tar.gz|tgz|tar.lzma|tlz|tar.xz|txz|tar.zst)
      # atool works for these, but we don't want to assume it is installed
      tar --list --verbose --file "${FILE_PATH}" && exit 5
      exit 1;;
    zip)
      unzip -l -P nopassword -- "${FILE_PATH}" && exit 5
      exit 1;;
    7z)
      7za l -p -- "${FILE_PATH}" && exit 5
      exit 1;;
    a|ace|alz|arc|arj|cab|cpio|deb|jar|lha|lzo|rar|rpm|rz|tar.Z|tZ|tar.7z|t7z|tzo|war|xpi)
      # use atool for less frequent archive types
      atool --list -- "${FILE_PATH}" && exit 5
      exit 1;;

    org) 
      env COLORTERM=8bit bat --color=always --style="plain" --line-range=:$MAXLN -- "${FILE_PATH}" && exit 5
      exit 1;;

    gpg|asc)

      gpg --list-only -d "${FILE_PATH}" 2>&1 && exit 5    # encrypted message
      gpg --list-only --list-packets "${FILE_PATH}" && exit 5   # key etc.
      exit 1;;

    torrent)
      transmission-show -- "${FILE_PATH}" && exit 5
      exit 1;;

  esac
}


handle_mime() {
  local mimetype="${1}"
  case "${mimetype}" in

    application/pdf)
      # convert first 10 pages to text
      mutool draw -F txt -i -- "${FILE_PATH}" 1-10 | fmt -w "${PV_WIDTH}" && exit 5
      exit 1;;

    text/html | application/xhtml+xml)
      w3m -dump "${FILE_PATH}" | trim; pipeexit 5
      lynx -dump -- "${FILE_PATH}" | trim; pipeexit 5
      pandoc -s -f html -t markdown -- "${FILE_PATH}" | trim; pipeexit 5
      exit 1;;

    ## compressed non-archive files (archives were already handled by extension)
    application/gzip)
      zcat "${FILE_PATH}" | trim ; pipeexit 5
      exit 1;;
    application/x-bzip2)
      bzcat "${FILE_PATH}" | trim ; pipeexit 5
      exit 1;;
    application/x-lzip)
      lzip -dc "${FILE_PATH}" | trim ; pipeexit 5
      exit 1;;
    application/x-xz | application/x-lzma)
      xzcat "${FILE_PATH}" | trim ; pipeexit 5
      exit 1;;
    application/zstd)
      zstdcat "${FILE_PATH}" | trim ; pipeexit 5
      exit 1;;

    ## openoffice: odp, ods, odt: text preview
    application/vnd.oasis.opendocument.presentation | \
    application/vnd.oasis.opendocument.spreadsheet | \
    application/vnd.oasis.opendocument.text )
      odt2txt "${FILE_PATH}" | trim; pipeexit 5
      ;;&
    
    ## new MS office formats: docx, pptx, xlsx
    application/vnd.openxmlformats-officedocument.wordprocessingml.document)
      catdocx "${FILE_PATH}" | trim; pipeexit 5
      ;;&
    application/vnd.openxmlformats-officedocument.spreadsheetml.sheet)
      xlsx2csv -- "${FILE_PATH}" | trim; pipeexit 5
      exit 1;;

    ## old MS office formats: doc, xls, rtf
    text/rtf | *msword)
      # note: catdoc does not always work for .doc files
      catdoc -- "${FILE_PATH}" | trim; pipeexit 5
      exit 1;;
    *ms-excel)
      xls2csv -- "${FILE_PATH}" | trim; pipeexit 5
      exit 1;;

    ## generic text / source code: use bat for syntax highlighting
    text/* | */xml | message/rfc822)
      env COLORTERM=8bit bat --color=always --style="plain" --line-range=:$MAXLN -- "${FILE_PATH}" && exit 5
      exit 2;;

    application/json)
      jq --color-output . "${FILE_PATH}" && exit 5
      # json.tool is packaged with python3 in nixpkgs
      python -m json.tool -- "${FILE_PATH}" && exit 5
      exit 2;;

    ## images, audio, video: show metadata
    image/*)
      # Preview as text conversion, DEPENDS: libcaca
      #img2txt --gamma=0.6 --width="${PV_WIDTH}" -- "${FILE_PATH}" && exit 4
      exiftool -g -All --File* --Directory --ExifToolVersion "${FILE_PATH}" && exit 5
      exit 1;;
    video/* | audio/*)
      mediainfo "${FILE_PATH}" && exit 5
      exiftool "${FILE_PATH}" && exit 5
      exit 1;;

    ## pandoc as slow fallback solution for some docs
    application/vnd.oasis.opendocument.text | \
    application/vnd.openxmlformats-officedocument.wordprocessingml.document | \
    application/epub+zip )
      pandoc -s -t markdown -- "${FILE_PATH}" | trim; pipeexit 5
      exit 1;;

  esac
}


handle_image() {
    ## Size of the preview if there are multiple options or it has to be
    ## rendered from vector graphics. If the conversion program allows
    ## specifying only one dimension while keeping the aspect ratio, the width
    ## will be used.
    local DEFAULT_SIZE="1920x1200"

    local mimetype="${1}"
    case "${mimetype}" in
      ## SVG
      image/svg+xml|image/svg)
        convert -- "${FILE_PATH}" "${IMAGE_CACHE_PATH}" && exit 6
        exit 1;;

      ## Image
      image/*)
        local orientation
        orientation="$( identify -format '%[EXIF:Orientation]\n' -- "${FILE_PATH}" )"
        ## If orientation data is present and the image actually
        ## needs rotating ("1" means no rotation)...
        if [[ -n "$orientation" && "$orientation" != 1 ]]; then
            ## ...auto-rotate the image according to the EXIF data.
            convert -- "${FILE_PATH}" -auto-orient "${IMAGE_CACHE_PATH}" && exit 6
        fi

        ## `w3mimgdisplay` will be called for all images (unless overriden as above), but might fail for unsupported types.
        exit 7;;

      ## Font
      application/font* | application/*opentype | font/* )
        preview_png="/tmp/$(basename "${IMAGE_CACHE_PATH%.*}").png"
        if fontimage -o "${preview_png}" \
                     --pixelsize "120" \
                     --fontname \
                     --pixelsize "80" \
                     --text "  ABCDEFGHIJKLMNOPQRSTUVWXYZ  " \
                     --text "  abcdefghijklmnopqrstuvwxyz  " \
                     --text "  0123456789.:,;(*!?') ff fl fi ffi ffl  " \
                     --text "  The quick brown fox jumps over the lazy dog.  " \
                     "${FILE_PATH}";
        then
          convert -- "${preview_png}" "${IMAGE_CACHE_PATH}" \
              && rm "${preview_png}" \
              && exit 6
        else
           exit 1
        fi
        ;;

    esac

}


handle_fallback() {
    echo '----- File Type Classification -----' && file --dereference -i --brief -- "${FILE_PATH}" && exit 5
    exit 1
}


handle_extension  # preview by extension first (fastest)
MIMETYPE="$( file --dereference --brief --mime-type -- "${FILE_PATH}" )"
if [[ "${PV_IMAGE_ENABLED}" == 'True' ]]; then
  handle_image "${MIMETYPE}"
fi
handle_mime "${MIMETYPE}"
handle_fallback
exit 1
