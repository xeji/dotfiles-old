#!/bin/sh
# preview script for the lf file manager

# Script arguments
f="${1}"         # full path of file to preview
pv_height="${2}" # lf r16 only sends preview height, seems to have changed in newer releases
# according to latest documentation these should be given:
#pv_width="${2}"     # width of preview pane (number of fitting characters)
#pv_height="${3}"    # height of preview pane (number of fitting characters)
#pv_hpos="${4}"      # horizontal position of preview pane
#pv_vpos="${5}"      # vertical position of preview pane


## DEPENDENCIES to preview various file types (nix packages)
# archiving : gnutar, unzip, p7zip, unrar
# bat : syntax highlighting for text + program code
# catdoc : .doc, .xls (includes xls2csv)
# catdocx : .docx
# compression : gzip, bzip2, xz, zstd
# exiftool : show image metdata
# haskellPackages.epub-tools : epub metadata
# jq : json (nicer, colored output)
# mediainfo : show media metadata
# mupdf : mutool (pdf -> text)
# odt2txt : text preview for libreoffice docs
# pandoc : (slow) fallback for html
# transmission : torrents
# w3m / lynx : html
# xlsx2csv : .xlsx

file_ext_orig="${f##*.}"    # file extension
file_ext="${file_ext_orig,,}"  # convert to lowercase


## match full filename for tar archives to identify tar.gz etc.
case "$f" in
  *.tar|*.tar.gz|*.tgz|*.tar.zst|*.tar.xz|*.txz|*.tar.bz2|*.tbz2)
    tar tvf "$f" | cut -d ' ' -f3- ;;
  *) false ;; # signal that there was no match
esac && exit 0


## match extension only for most file types to avoid lots of regex matches
case "$file_ext" in

  # most frequently used
  pdf) mutool draw -F txt -i -- "$f" 1-10 ;;
  log|md|org|py|sh|tex) 
    bat --color=always --style="plain" -- "$f" ;;

  # office docs
  odt|ods|odp|sxw) odt2txt "$f" ;;
  docx) catdocx "$f" - ;;
  xlsx) xlsx2csv -- "$f" ;;
  doc|rtf) catdoc -- "$f" ;;
  xls) xls2csv -- "$f" ;;

  html|htm|xhtml) # try several options
      w3m -dump "$f" || \
      lynx -dump -- "$f" || \
      pandoc -s -f html -t markdown -- "$f" ;;

  gpg|asc)  # try encrypted mesg first, then key
    gpg --list-only -d -- "$file" 2>&1 || \
    gpg --list-only --list-packets -- "$file" ;; # key etc.

  # non-tar archives
  zip|jar|war|ear|oxt) unzip -l -P nopassword -- "$f" ;;
  7z) 7za l -p -- "$f" ;;
  rar) unrar l -- "$f" ;;

  # compressed non-archive files (archives were already handled by extension)
  gz)  zcat -- "$f" ;;
  bz2) bzcat -- "$f" ;;
  xz)  xzcat -- "$f" ;;
  zst) zstdcat -- "$f" ;;

  # other
  epub) epubmeta  -- "$f" ;;
  torrent) transmission-show "$f" ;;
  [1-8]) man "$f" | col -b ;;
  iso) iso-info --no-header -l "$f" ;;

  *) false ;; # signal that there was no match

esac && exit 0

## unknown extension -> use mime type
mimetype="$( file --dereference --brief --mime-type -- "$f" )"
case "$mimetype" in

  text/*) bat --color=always --style="plain" -- "$f" ;;
  
  application/json)
    # json.tool is packaged with python3 in nixpkgs
    jq --color-output . "$f" ||
    python -m json.tool "$f" ;;

  # images, audio, video: show metadata
  image/*) exiftool -g -All --File* --Directory --ExifToolVersion "$f" ;;
  video/*|audio/*) mediainfo "$f" || exiftool "$f" ;;

  # fallback: just print mime type
  *) echo "File Type:  $mimetype" ;;

esac
