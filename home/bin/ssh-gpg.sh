#!/bin/bash
# set up this shell to use gpg-agent as ssh agent
# since we do not want gpg-agent to manage all ssh keys
# this must be sourced - not executed
# UB 2015-02-09, modified 2016-07-01


# make sure gpg-agent is running
(gpg-connect-agent /bye 2>/dev/null) || gpg-agent --daemon


gpg_agent_auth_sock=${XDG_RUNTIME_DIR}/gnupg/S.gpg-agent.ssh

if [[ "$SSH_AUTH_SOCK" == "$gpg_agent_auth_sock" ]]; then
  export SSH_AUTH_SOCK=$SSH_AGENT_AUTH_SOCK
else
  export SSH_AGENT_AUTH_SOCK=$SSH_AUTH_SOCK
  export SSH_AUTH_SOCK=$gpg_agent_auth_sock
fi
echo "ssh auth now on: $SSH_AUTH_SOCK"
