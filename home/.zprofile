# start x/sway automatically on some machines
if systemctl -q is-active graphical.target && [[ ! $DISPLAY && $XDG_VTNR -eq 1 ]]; then
  case "$HOST" in
    joi | robo | leon ) 
      exec startx
      ;;
    ace )
      exec sway
      ;;
  esac
fi
